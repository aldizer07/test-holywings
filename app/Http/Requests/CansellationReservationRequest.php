<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CansellationReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reservation_id' => 'required|exists:reservations,id',
            'cansellation_reason_id' => 'required|exists:cansellation_reasons,id',
            'description'   => 'required|string|min:1',
        ];
    }
}
