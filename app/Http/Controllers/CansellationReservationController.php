<?php

namespace App\Http\Controllers;

use App\Http\Requests\CansellationReservationRequest;
use App\Http\Resources\CansellationReservationResource;
use App\Models\CansellationReason;
use App\Models\Reservation;
use App\Models\ReservationCansellation;

class CansellationReservationController extends Controller
{

    public function cansellationReservationStore(CansellationReservationRequest $request)
    {
        $data = $request->validated();

        ReservationCansellation::create($data);

        $cansellation = new CansellationReservationResource($data);

        return response()->json([
            'message' => 'success',
            'code'  => 200,
            'data'  => $cansellation
        ]);
    }
}
