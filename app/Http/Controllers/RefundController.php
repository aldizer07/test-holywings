<?php

namespace App\Http\Controllers;

use App\Models\Refund;
use Illuminate\Http\Request;

class RefundController extends Controller
{
    public function getAll()
    {
        $data = Refund::get();

        return response()->json($data);
    }
}
