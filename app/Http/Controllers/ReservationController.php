<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReservationResource;
use App\Models\Reservation;
use DateTime;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function show($id)
    {
        $reservation = Reservation::with(['tabel','floor'])->find($id);

        return response()->json(new ReservationResource($reservation));

    }

    public function getTotalAmountCansellation($reservation_id)
    {
        $reservation = new ReservationResource(Reservation::with(['tabel','floor'])->find($reservation_id));

        $end = new DateTime();
        $start = new DateTime($reservation->date);
        $diff = $start->diff($end);
        $penalty = 0;
        $text = "0%";

        $hour = $diff->h;

        if ($hour > 24 && $hour < 48) {
            $penalty = (int) $reservation->price * 0.5;
            $text = "50%";
        }

        $total = (int) $reservation->price - $penalty;

        $data = [
            'text'  => $text,
            'penalty' => $penalty,
            'total' => $penalty == 0 ? $reservation->price : $total,
        ];

        return response()->json($data);
    }
}
