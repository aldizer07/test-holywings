<?php

namespace App\Http\Controllers;

use App\Http\Resources\CansellationResource;
use App\Models\CansellationReason;
use Illuminate\Http\Request;

class CansellationController extends Controller
{
    public function getAll()
    {
        $data = [
            'message' => 'success',
            'code'  => 200,
            'data'  => new CansellationResource(CansellationReason::get())
        ];
        return response()->json($data);
    }
}
