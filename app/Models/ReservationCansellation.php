<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationCansellation extends Model
{
    use HasFactory;
    protected $fillable = ['reservation_id','cansellation_reason_id','description'];


    public function reservation()
    {
        return $this->belongsTo(Reservation::class, 'reservation_id');
    }

    public function cansellation_reason()
    {
        return $this->belongsTo(CansellationReason::class, 'cansellation_reason_id');
    }
}
