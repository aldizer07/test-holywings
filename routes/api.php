<?php

use App\Http\Controllers\CansellationController;
use App\Http\Controllers\CansellationReservationController;
use App\Http\Controllers\RefundController;
use App\Http\Controllers\ReservationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/reservation/{id}',[ReservationController::class, 'show']);
Route::post('/cansellation-reservation',[CansellationReservationController::class, 'cansellationReservationStore']);
Route::get('/refunds',[RefundController::class, 'getAll']);
Route::get('getTotalAmountCansellation/{reservation_id}',[ReservationController::class, 'getTotalAmountCansellation']);
Route::get('/cansellation-reason',[CansellationController::class, 'getAll']);
