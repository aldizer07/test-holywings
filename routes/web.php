<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $x = 20;
    for ($i=0; $i < $x ; $i++) {
        $tiga = $i % 3;
        $lima = $i % 5;
        if ( $tiga== 0) {
            echo "Tiga <br>";
        }elseif($lima == 0 ) {
            echo "Lima <br>";
        }elseif($tiga == 0 && $lima == 0){
            echo "TigaLima <br>";
        }else{
            echo $i."<br>";
        }
    }
});
