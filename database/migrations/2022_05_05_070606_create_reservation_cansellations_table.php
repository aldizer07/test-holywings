<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationCansellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_cansellations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reservation_id');
            $table->unsignedBigInteger('cansellation_reason_id');
            $table->string('description');
            $table->timestamps();

            $table->foreign('reservation_id')->on('reservations')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cansellation_reason_id')->on('cansellation_reasons')->references('id')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_cansellations');
    }
}
