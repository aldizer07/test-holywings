<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('store_id');
            $table->date('date');
            $table->unsignedBigInteger('table_id');
            $table->unsignedBigInteger('floor_id');
            $table->string('price');
            $table->string('pinalty')->default("50");
            $table->timestamps();

            $table->foreign('store_id')->on('stores')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('table_id')->on('tables')->references('id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('floor_id')->on('floors')->references('id')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
