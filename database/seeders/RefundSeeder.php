<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('refunds')->insert([
            'name' => '50% Refund',
            'description' => "desc",
        ]);

        DB::table('refunds')->insert([
            'name' => 'No Refund',
            'description' => "desc",
        ]);

        DB::table('refunds')->insert([
            'name' => 'Refund Process',
            'description' => "desc",
        ]);
    }
}
